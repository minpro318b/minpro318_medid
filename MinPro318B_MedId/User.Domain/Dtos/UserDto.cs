﻿namespace User.Domain.Dtos
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public Guid? Biodata_Id { get; set; }
        public Guid? Role_Id { get; set; }
        public string Email { get; set; } = default!;
        public string Password { get; set; } = default!;
        public UserStatusEnum Status { get; set; } = default!;
    }
}
