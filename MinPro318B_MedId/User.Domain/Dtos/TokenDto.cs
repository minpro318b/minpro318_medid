﻿namespace User.Domain.Dtos
{
    public class TokenDto
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public DateTime Expired_On { get; set; }
        public string Used_For { get; set; }
        public UserStatusEnum Status { get; set; }
    }
}
