﻿namespace User.Domain.Dtos
{
    public class BiodataDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; } = default!;
        public string Mobile_Phone { get; set; } = default!;
        public string? Image_Path { get; set; } = default!;
        public UserStatusEnum Status { get; set; } = default!;
    }
}
