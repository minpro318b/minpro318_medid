﻿using System.ComponentModel.DataAnnotations;

namespace User.Domain.Dtos
{
    public class EmailDto
    {
        [Required,EmailAddress(ErrorMessage = "Please input a valid email address")]
        public string To { get; set; } = string.Empty;
        public string Subject { get; set; } = string.Empty;
        public string Body { get; set; }
    }
}
