﻿namespace User.Domain.Dtos
{
    public class RoleDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;
        public string Code { get; set; } = default!;
        public UserStatusEnum Status { get; set; } = default!;
    }
}
