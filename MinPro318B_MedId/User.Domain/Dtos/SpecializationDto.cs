﻿namespace User.Domain.Dtos
{
    public class SpecializationDto
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public StatusEnum Status { get; set; }
        public Guid Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public Guid? Modified_By { get; set; }
        public DateTime Modified_On { get; set; }
    }
}
