﻿using System.Text.Json.Serialization;

namespace User.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum StatusEnum
    {
        Active,
        Removed
    }
}
