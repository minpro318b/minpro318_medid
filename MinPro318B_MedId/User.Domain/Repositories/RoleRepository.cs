﻿
using Microsoft.EntityFrameworkCore;
using User.Domain.Entities;

namespace User.Domain.Repositories
{
    public interface IRoleRepository
    {
        Task<IEnumerable<RoleEntity>> GetAll();
        Task<RoleEntity> GetById(Guid id);
        Task<RoleEntity> GetByName(string name);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        Task<RoleEntity> Update(RoleEntity entity);
        Task<RoleEntity> Add(RoleEntity entity);
    }
    public class RoleRepository : IRoleRepository
    {
        protected readonly UserDbContext _context;
        public RoleRepository(UserDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<RoleEntity> Add(RoleEntity entity)
        {
            _context.Set<RoleEntity>().Add(entity);
            return entity;
        }

        public async Task<IEnumerable<RoleEntity>> GetAll()
        {
            return await _context.Set<RoleEntity>().Where(x => !x.Status.Equals(UserStatusEnum.Removed)).ToListAsync();
        }

        public async Task<RoleEntity> GetById(Guid id)
        {
            return await _context.Set<RoleEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
        public async Task<RoleEntity> Update(RoleEntity entity)
        {
            _context.Set<RoleEntity>().Update(entity);
            return entity;
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        public async Task<RoleEntity> GetByName(string name)
        {
            return await _context.Set<RoleEntity>().FirstOrDefaultAsync(x => x.Name == name);
        }
    }
}
