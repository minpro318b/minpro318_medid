﻿using Microsoft.EntityFrameworkCore;
using User.Domain.Entities;
using static System.Net.WebRequestMethods;

namespace User.Domain.Repositories
{
    public interface ITokenRepository
    {
        Task<IEnumerable<TokenEntity>> GetAll();
        Task<TokenEntity> GetOtp(string otp);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        Task<TokenEntity> Update(TokenEntity entity);
        Task<TokenEntity> Add(TokenEntity entity);
        Task<TokenEntity> GetByEmail(string email);
    }
    public class TokenRepository : ITokenRepository
    {
        private readonly UserDbContext _context;

        public TokenRepository(UserDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<TokenEntity> Add(TokenEntity entity)
        {
           _context.Set<TokenEntity>().Add(entity);
            return entity;
        }

        public async Task<IEnumerable<TokenEntity>> GetAll()
        {
            return await _context.Set<TokenEntity>().Where(x => !x.Status.Equals(UserStatusEnum.Removed)).ToListAsync();
        }

        public async Task<TokenEntity> GetOtp(string otp)
        {
            return await _context.Set<TokenEntity>().FirstOrDefaultAsync(x => x.Token == otp);
        }
        public async Task<TokenEntity> GetByEmail(string email)
        {
            return await _context.Set<TokenEntity>().FirstOrDefaultAsync(x => x.Email == email);
        }
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
        public async Task<TokenEntity> Update(TokenEntity entity)
        {
            _context.Set<TokenEntity>().Update(entity);
            return entity;
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
