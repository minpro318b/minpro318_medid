﻿using Microsoft.EntityFrameworkCore;
using User.Domain.Entities;

namespace User.Domain.Repositories
{
    public interface ISpecializationRepository
    {
        Task<IEnumerable<SpecializationEntity>> GetAll();
        Task<SpecializationEntity> GetById(Guid id);
        Task<SpecializationEntity> Add(SpecializationEntity entity);
        Task<SpecializationEntity> Update(SpecializationEntity entity);
        void Delete(SpecializationEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class SpecializationRepository : ISpecializationRepository
    {
        protected readonly UserDbContext _context;

        public SpecializationRepository(UserDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<SpecializationEntity> Add(SpecializationEntity entity)
        {
            _context.Set<SpecializationEntity>().Add(entity);
            return entity;
        }

        public void Delete(SpecializationEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<SpecializationEntity>> GetAll()
        {
            return await _context.Set<SpecializationEntity>().ToListAsync();
        }

        public async Task<SpecializationEntity> GetById(Guid id)
        {
            return await _context.Set<SpecializationEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<SpecializationEntity> Update(SpecializationEntity entity)
        {
            _context.Set<SpecializationEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
