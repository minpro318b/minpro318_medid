﻿using Microsoft.EntityFrameworkCore;
using User.Domain.Entities;

namespace User.Domain.Repositories
{
    public interface IBiodataRepository
    {
        Task<IEnumerable<BiodataEntity>> GetAll();
        Task<BiodataEntity> GetById(Guid id);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        Task<BiodataEntity> Update(BiodataEntity entity);
        Task<BiodataEntity> Add(BiodataEntity entity);
    }
    public class BiodataRepository : IBiodataRepository
    {
        protected readonly UserDbContext _context;

        public BiodataRepository(UserDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<IEnumerable<BiodataEntity>> GetAll()
        {
            return await _context.Set<BiodataEntity>().Where(x => !x.Status.Equals(UserStatusEnum.Removed)).ToListAsync();
        }
        public async Task<BiodataEntity> GetById(Guid id)
        {
            return await _context.Set<BiodataEntity>().FindAsync(id);
        }
        public async Task<BiodataEntity> Add(BiodataEntity entity)
        {
            _context.Set<BiodataEntity>().Add(entity);
            return entity;
        }
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }
        public async Task<BiodataEntity> Update(BiodataEntity entity)
        {
             _context.Set<BiodataEntity>().Update(entity);
            return entity;
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }
}
