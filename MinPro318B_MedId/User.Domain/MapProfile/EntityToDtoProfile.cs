﻿using AutoMapper;
using User.Domain.Dtos;
using User.Domain.Entities;

namespace User.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base ("Entity to Dto Profile")
        {
            //User Mapping
            CreateMap<UserEntity, UserDto>();
            CreateMap<UserDto, UserEntity>();

            //Biodata Mapping
            CreateMap<BiodataEntity, BiodataDto>();
            CreateMap<BiodataDto, BiodataEntity>();

            //Role Mapping
            CreateMap<RoleEntity, RoleDto >();
            CreateMap<RoleDto, RoleEntity >();

            //Token Mapping
            CreateMap<TokenEntity, TokenDto>();
            CreateMap<TokenDto, TokenEntity >();
        }
    }
}
