﻿
namespace User.Domain.Entities
{
    public class BiodataEntity
    {
        public Guid Id { get; set; }
        public string FullName { get; set; } = default!;
        public string Mobile_Phone { get; set; } = default!;
        public string Image_Path { get; set; } = default!;

        //Base Properties
        public Guid Created_By { get; set; }
        public DateTime Created_On { get; internal set; } = DateTime.Now;
        public Guid? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public UserStatusEnum Status { get; set; } = default!;
    }
}
