﻿
using System.Numerics;

namespace User.Domain.Entities
{
    public class UserEntity
    {
        public Guid Id { get; set; }
        public Guid? Biodata_Id { get; set; }
        public Guid? Role_Id { get; set; }
        public string Email { get; set; } = default!;
        public string Password { get; set; } = default!;
        public int Login_Attempt { get; set; } = default!;
        public DateTime Last_Login { get; set; }

        //Base Properties
        public Guid Created_By { get; set; }
        public DateTime Created_On { get; internal set; } = DateTime.Now;
        public Guid? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public UserStatusEnum Status { get; set; } = default!;
    }
}
