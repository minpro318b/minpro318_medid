﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace User.Domain.Entities.Configurations
{
    public class SpecializationConfiguration : IEntityTypeConfiguration<SpecializationEntity>
    {
        public void Configure(EntityTypeBuilder<SpecializationEntity> builder)
        {
            builder.ToTable("M_Specialization");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(50).IsRequired(false);
            builder.Property(e => e.Status).IsRequired();
            builder.Property(e => e.Created_By).IsRequired();
            builder.Property(e => e.Created_On).IsRequired();
            builder.Property(e => e.Modified_By).IsRequired(false);
            builder.Property(e => e.Modified_On).IsRequired(false);
        }
    }
}
