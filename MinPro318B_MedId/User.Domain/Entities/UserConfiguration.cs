﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace User.Domain.Entities
{
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("M_User");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Biodata_Id).IsRequired(false);
            builder.Property(x => x.Role_Id).IsRequired(false);
            builder.Property(x => x.Email).IsRequired(false).HasMaxLength(100);
            builder.Property(x => x.Password).IsRequired(false).HasMaxLength(255);
            builder.Property(x => x.Login_Attempt);
            builder.Property(x => x.Last_Login);
            builder.Property(x => x.Status).IsRequired();
            builder.Property(x => x.Modified_By).IsRequired(false);
            builder.Property(x => x.Modified_On).IsRequired(false);
        }
    }
    public class BiodataConfiguration : IEntityTypeConfiguration<BiodataEntity>
    {
        public void Configure(EntityTypeBuilder<BiodataEntity> builder)
        {
            builder.ToTable("M_Biodata");
            builder.HasKey(b => b.Id);
            builder.Property(b => b.FullName).IsRequired(false).HasMaxLength(255);
            builder.Property(b => b.Mobile_Phone).IsRequired(false).HasMaxLength(15);
            builder.Property(b => b.Image_Path).IsRequired(false).HasMaxLength(255);
            builder.Property(b => b.Status).IsRequired();
            builder.Property(b => b.Modified_By).IsRequired(false);
            builder.Property(b => b.Modified_On).IsRequired(false);
        }
    }
    public class RoleConfiguration : IEntityTypeConfiguration<RoleEntity>
    {
        public void Configure(EntityTypeBuilder<RoleEntity> builder)
        {
            builder.ToTable("M_Role");
            builder.HasKey(r => r.Id);
            builder.Property(r => r.Name).IsRequired(false).HasMaxLength(20);
            builder.Property(r => r.Code).IsRequired(false).HasMaxLength(20);
            builder.Property(r => r.Status).IsRequired();
            builder.Property(r => r.Modified_By).IsRequired(false);
            builder.Property(r => r.Modified_On).IsRequired(false);
        }
    }
    public class TokenConfiguration : IEntityTypeConfiguration<TokenEntity>
    {
        public void Configure(EntityTypeBuilder<TokenEntity> builder)
        {
            builder.ToTable("T_Token");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Email).IsRequired(false).HasMaxLength(100);
            builder.Property(t => t.User_Id).IsRequired(false);
            builder.Property(t => t.Token).IsRequired(false).HasMaxLength(50);
            builder.Property(t => t.Expired_On).IsRequired(false);
            builder.Property(t => t.Used_For).IsRequired(false);
            builder.Property(r => r.Status).IsRequired();
            builder.Property(r => r.Modified_By).IsRequired(false);
            builder.Property(r => r.Modified_On).IsRequired(false);
        }
    }
}
