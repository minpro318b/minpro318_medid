﻿namespace User.Domain.Entities
{
    public class TokenEntity
    {
        public int Id { get; set; }
        public string? Email { get; set; }
        public Guid? User_Id { get; set; }
        public string Token { get; set; }
        public DateTime? Expired_On { get; set; }
        public string? Used_For { get; set; } 
        //Base Properties
        public Guid Created_By { get; set; }
        public DateTime Created_On { get; internal set; } = DateTime.Now;
        public Guid? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public UserStatusEnum Status { get; set; } = default!;
    }
}
