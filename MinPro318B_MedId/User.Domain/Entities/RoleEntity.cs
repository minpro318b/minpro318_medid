﻿using System.Numerics;

namespace User.Domain.Entities
{
    public class RoleEntity
    {   
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;
        public string Code { get; set; } = default!;
        //Base Properties
        public Guid Created_By { get; set; }
        public DateTime Created_On { get; internal set; } = DateTime.Now;
        public Guid? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public UserStatusEnum Status { get; set; } = default!;
    }
}
