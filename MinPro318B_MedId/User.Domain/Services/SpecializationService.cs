﻿using AutoMapper;
using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.Repositories;

namespace User.Domain.Services
{
    public interface ISpecializationService
    {
        Task<IEnumerable<SpecializationDto>> All();
        Task<SpecializationDto> GetSpecializationById(Guid id);
        Task<SpecializationDto> AddSpecialization(SpecializationDto dto);
        Task<bool> UpdateSpecialization(Guid id, SpecializationDto dto);
        Task<bool> UpdateSpecializationStatus(Guid id, StatusEnum status);
    }
    public class SpecializationService : ISpecializationService
    {
        private ISpecializationRepository _repository;
        private readonly IMapper _mapper;

        public SpecializationService(ISpecializationRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<SpecializationDto> AddSpecialization(SpecializationDto dto)
        {
            if (dto != null)
            {
                dto.Status = StatusEnum.Active;
                dto.Created_By = new Guid("C8A73C67-2002-4422-A361-43287D5CA979");
                dto.Created_On = DateTime.Now;
                var dtoToEntity = _mapper.Map<SpecializationEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<SpecializationDto>(entity);
                }
            }

            return new SpecializationDto();
        }

        public async Task<IEnumerable<SpecializationDto>> All()
        {
            return _mapper.Map<IEnumerable<SpecializationDto>>(await _repository.GetAll());
        }

        public async Task<SpecializationDto> GetSpecializationById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<SpecializationDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateSpecialization(Guid id, SpecializationDto dto)
        {
            var specialization = await _repository.GetById(id);
            
            if (specialization != null)
            {
                var entity = await _repository.Update(_mapper.Map<SpecializationEntity>(dto));
                entity.Created_By = specialization.Created_By;
                entity.Created_On = specialization.Created_On;
                entity.Modified_By = new Guid("0C00464C-A598-4DCF-8DAE-68487DDD467C");
                entity.Modified_On = DateTime.Now;

                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public async Task<bool> UpdateSpecializationStatus(Guid id, StatusEnum status)
        {
            var specialization = await _repository.GetById(id);

            if (specialization != null)
            {
                specialization.Status = status;
                specialization.Modified_By = new Guid("40D711BF-3577-401E-A357-3EC6D5A7B90B");
                specialization.Modified_On = DateTime.Now;
                var entity = await _repository.Update(specialization);
                //entity.Created_By = specialization.Created_By;
                //entity.Created_On = specialization.Created_On;
                //entity.Modified_By = new Guid("0C00464C-A598-4DCF-8DAE-68487DDD467C");
                //entity.Modified_On = DateTime.Now;

                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
