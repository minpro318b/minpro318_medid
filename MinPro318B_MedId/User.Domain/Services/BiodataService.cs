﻿using AutoMapper;
using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.Repositories;

namespace User.Domain.Services
{
    public interface IBiodataService
    {
        Task<IEnumerable<BiodataDto>> All();
        Task<BiodataDto?> GetBiodataById(Guid id);
        Task<BiodataDto> AddBiodata(BiodataDto dto);
        Task<bool>UpdateBiodata(BiodataDto dto);
        Task<bool> ActivateBiodata(Guid id);
        Task<bool> DeleteBiodata(Guid id);
    }
    public class BiodataService : IBiodataService
    {
        private readonly IBiodataRepository _repository;
        private readonly IMapper _mapper;

        public BiodataService(IBiodataRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<IEnumerable<BiodataDto>> All()
        {
            return _mapper.Map<IEnumerable<BiodataDto>>(await _repository.GetAll());
        }

        public async Task<BiodataDto?> GetBiodataById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if(result != null)
                    return _mapper.Map<BiodataDto>(result);
            }
           return null;
        }
        public async Task<BiodataDto> AddBiodata(BiodataDto dto)
        {
            if(dto != null)
            {
                dto.Status = UserStatusEnum.Inactive;
                var entity = await _repository.Add(_mapper.Map<BiodataEntity>(dto));
                var result = await _repository.SaveChangesAsync();
                if (result > 0)
                    return _mapper.Map<BiodataDto>(entity);
            }
            return new BiodataDto();
        }
        public async Task<bool>UpdateBiodata(BiodataDto dto)
        {
            if (dto != null)
            {
                var biodata = await _repository.GetById(dto.Id);
                if(biodata != null)
                {
                    biodata.Modified_On = DateTime.Now;
                    var entity = await _repository.Update(_mapper.Map<BiodataEntity>(dto));
                    entity.Status = biodata.Status;
                    var result = await _repository.SaveChangesAsync();
                    if (result > 0) return true;
                }
            }
            return false;
        }
        public async Task<bool>ActivateBiodata(Guid id)
        {
            var biodata = await _repository.GetById(id);
            if(biodata != null)
            {
                if(biodata.Status == UserStatusEnum.Inactive)
                {
                    biodata.Status = UserStatusEnum.Active;
                }
                else
                    biodata.Status = UserStatusEnum.Inactive;
                biodata.Modified_On = DateTime.Now;
                var entity = await _repository.Update(_mapper.Map<BiodataEntity>(biodata));
                var result = await _repository.SaveChangesAsync();
                if (result > 0) return true;
            }
            return false;
        }
        public async Task<bool> DeleteBiodata(Guid id)
        {
            var biodata = await _repository.GetById(id);
            if (biodata != null)
            {
                biodata.Status = UserStatusEnum.Removed;
                biodata.Modified_On = DateTime.Now;
                var entity = await _repository.Update(_mapper.Map<BiodataEntity>(biodata));
                var result = await _repository.SaveChangesAsync();
                if (result > 0) return true;
            }
            return false;
        }
    }
}
