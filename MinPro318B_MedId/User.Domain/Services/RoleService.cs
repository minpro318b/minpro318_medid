﻿
using AutoMapper;
using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.Repositories;

namespace User.Domain.Services
{
    public interface IRoleService
    {
        Task<IEnumerable<RoleDto>> All();
        Task<RoleDto?> GetRoleById(Guid id);
        Task<RoleDto?> GetRoleByName(string name);
        Task<RoleDto> AddRole(RoleDto dto);
        Task<bool> UpdateRole(RoleDto dto);
        Task<bool> ActivateRole(Guid id);
        Task<bool> DeleteRole(Guid id);
    }
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _repository;
        private readonly IMapper _mapper;

        public RoleService(IRoleRepository repository,IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<bool> ActivateRole(Guid id)
        {
            var role = await _repository.GetById(id);
            if(role != null)
            {
                if (role.Status == UserStatusEnum.Inactive)
                {
                    role.Status = UserStatusEnum.Active;
                }
                else
                    role.Status = UserStatusEnum.Inactive;
                role.Modified_On = DateTime.Now;
                var entity = await _repository.Update(_mapper.Map<RoleEntity>(role));
                var result = await _repository.SaveChangesAsync();
                if (result > 0) return true;
            }
            return false;
        }

        public async Task<RoleDto> AddRole(RoleDto dto)
        {
            if(dto != null)
            {
                dto.Status = UserStatusEnum.Inactive;
                var entity = await _repository.Add(_mapper.Map<RoleEntity>(dto));
                var result = await _repository.SaveChangesAsync();
                if (result > 0) return _mapper.Map<RoleDto>(entity);
            }
            return new RoleDto();
        }

        public async Task<IEnumerable<RoleDto>> All()
        {
            return _mapper.Map<IEnumerable<RoleDto>>(await _repository.GetAll());
        }

        public async Task<bool> DeleteRole(Guid id)
        {
            var role = await _repository.GetById(id);
            if(role != null)
            {
                role.Status = UserStatusEnum.Removed;
                role.Modified_On = DateTime.Now;
                var entity = await _repository.Update(_mapper.Map<RoleEntity>(role));
                var result = await _repository.SaveChangesAsync();
                if (result > 0) return true;
            }
            return false;
        }

        public async Task<RoleDto?> GetRoleById(Guid id)
        {
            if(id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                    return _mapper.Map<RoleDto>(result);
            }
            return null;
        }
        public async Task<RoleDto?> GetRoleByName(string name)
        {
            var result = await _repository.GetByName(name);
            if (result != null)
                return _mapper.Map<RoleDto>(result);
            return null;
        }

        public async Task<bool> UpdateRole(RoleDto dto)
        {
            var role = await _repository.GetById(dto.Id);
            if(role != null)
            {
                role.Modified_On = DateTime.Now;
                var entity = await _repository.Update(_mapper.Map<RoleEntity>(dto));
                entity.Status = role.Status;
                var result = await _repository.SaveChangesAsync();
                if (result > 0) return true;
            }
            return false;
        }
    }
}
