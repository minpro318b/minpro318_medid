﻿using User.Domain.Dtos;
using AutoMapper;
using User.Domain.Repositories;
using System.Security.Cryptography;
using User.Domain.Entities;
using User.Domain.MapProfile;

namespace User.Domain.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> All();
        Task<UserDto?> GetUserById(Guid id);
        Task<bool> IsEmailExist(string email);
        int[] GenerateOTP(int digits);
        Task<TokenDto> StoreOTP(TokenDto dto);
        Task<bool> CheckOtp(string otp);
        Task<UserDto> AddUser(UserDto dto);
    }
    public class UserService : IUserService
    {
        private IUserRepository _repository;
        private readonly IMapper _mapper;
        private ITokenRepository _tokenRepository;
        public UserService(IUserRepository repository,
                           ITokenRepository tokenRepository,
                           IMapper mapper)
        {
            _repository = repository;
            _tokenRepository = tokenRepository;
            _mapper = mapper;
        }
        public async Task<IEnumerable<UserDto>> All()
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _repository.GetAll());
        }
        public async Task<UserDto?> GetUserById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                    return _mapper.Map<UserDto>(result);
            }
            return null;
        }
        public async Task<bool> IsEmailExist(string email)
        {
            var result = await _repository.GetEmail(email);
            return result != null;
        }
        public async Task<UserDto> AddUser(UserDto dto)
        {
            if(dto != null)
            {
                dto.Status = UserStatusEnum.Active;
                var entity = await _repository.Add(_mapper.Map<UserEntity>(dto));
                var result = await _repository.SaveChangesAsync();
                if (result > 0)
                    return _mapper.Map<UserDto>(entity);
            }
            return new UserDto();
        }
        public async Task<bool> CheckOtp(string otp)
        {
            var entity = await _tokenRepository.GetOtp(otp);
            if (entity != null)
            {
                DateTime useOtp = DateTime.Now;
                TimeSpan? intervalOtp = entity.Expired_On - useOtp;

                if (intervalOtp.Value.Minutes < 0 && entity.Status.Equals(UserStatusEnum.Active))
                {
                    entity.Status = UserStatusEnum.Inactive;
                    entity.Modified_On = useOtp;
                    await _tokenRepository.Update(_mapper.Map<TokenEntity>(entity));
                    await _tokenRepository.SaveChangesAsync();
                    return true;
                }

            }
            return false;
        }
        public int[] GenerateOTP(int digits)
        {
            var arr = new int[digits];
            for(int i = 0; i < digits; i++)
            {
                arr[i] = RandomNumberGenerator.GetInt32(0, 9);
            }
            return arr;
        }
        public async Task<TokenDto?> StoreOTP(TokenDto dto)
        {
            var repeatToken = await _tokenRepository.GetByEmail(dto.Email);
            if(dto != null)
            {
                if(repeatToken != null)
                {
                    TimeSpan? intervalToken = repeatToken.Expired_On - DateTime.Now;
                    if (intervalToken.Value.Minutes < 7)
                    {
                        repeatToken.Status = UserStatusEnum.Inactive;
                        repeatToken.Modified_On = DateTime.Now;
                        dto.Status = UserStatusEnum.Active;
                    }
                    else
                        return null;
                    
                }
                else
                {
                    dto.Status = UserStatusEnum.Active;
                }
                var entity = await _tokenRepository.Add(_mapper.Map<TokenEntity>(dto));
                var result = await _tokenRepository.SaveChangesAsync();
                if (result > 0) return _mapper.Map<TokenDto>(entity);
            }
            return null;
        }
    }
}
