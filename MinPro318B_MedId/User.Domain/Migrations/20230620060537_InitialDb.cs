﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace User.Domain.Migrations
{
    /// <inheritdoc />
    public partial class InitialDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "M_Biodata",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Mobile_Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Image_Path = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Created_By = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Created_On = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_By = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Modified_On = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_Biodata", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "M_Role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Code = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Created_By = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Created_On = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_By = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Modified_On = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "M_User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Biodata_Id = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Role_Id = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Login_Attempt = table.Column<int>(type: "int", nullable: false),
                    Last_Login = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Created_By = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Created_On = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_By = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Modified_On = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_M_User", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "M_Biodata");

            migrationBuilder.DropTable(
                name: "M_Role");

            migrationBuilder.DropTable(
                name: "M_User");
        }
    }
}
