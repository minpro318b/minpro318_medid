﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace User.Domain.Migrations
{
    /// <inheritdoc />
    public partial class add_token_table : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "T_Token",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    User_Id = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Token = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Expired_On = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Used_For = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Created_By = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Created_On = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Modified_By = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Modified_On = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Token", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "T_Token");
        }
    }
}
