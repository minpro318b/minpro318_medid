﻿using System.ComponentModel.DataAnnotations;

namespace User.GraphQL.Inputs
{
    public class RegistrationInput
    {
        [Required , EmailAddress(ErrorMessage = "Please input a valid email address")]
        public string Email { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required,MinLength(12,ErrorMessage = "Please input a valid phone number")]
        public string Mobile_Phone { get; set; }
        public string Role_Name { get; set; }
    }
}

