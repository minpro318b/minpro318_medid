﻿using System.ComponentModel.DataAnnotations;

namespace User.GraphQL.Inputs
{
    public class PasswordInput
    {
        [Required,MinLength(8,ErrorMessage = "Password must be atleast 8 characters"),CustomValidation(typeof(PasswordValidation),nameof(PasswordValidation.ValidatePassword))]
        public string Password { get; set; } = default!;
        [Required,Compare("Password")]
        public string ConfirmedPassword { get; set; } = default!;
    }
    public class PasswordValidation
    {
        public static ValidationResult ValidatePassword(string password)
        {
            int validConditions = 0;
            foreach (char c in password)
            {
                if (c >= 'a' && c <= 'z')
                {
                    validConditions++;
                    break;
                }
            }
            foreach (char c in password)
            {
                if (c >= 'A' && c <= 'Z')
                {
                    validConditions++;
                    break;
                }
            }
            if (validConditions == 0) return new ValidationResult("Password must consist atleast one lowercase and one Uppercase letter");
            foreach (char c in password)
            {
                if (c >= '0' && c <= '9')
                {
                    validConditions++;
                    break;
                }
            }
            if (validConditions == 1) return new ValidationResult("Password must consist atleast one number");
            if (validConditions == 2)
            {
                char[] special = { '@', '#', '$', '%', '^', '&', '+', '=' }; // or whatever    
                if (password.IndexOfAny(special) == -1) return new ValidationResult("Password must consist atleast one special character such as : ('@', '#', '$', '%', '^', '&', '+', '=')");
            }
            return ValidationResult.Success;
        }
    }
    
}
