﻿namespace User.GraphQL.Schema.Inputs
{
    public class RoleInput
    {
        public string Name { get; set; } = default!;
        public string Code { get; set; } = default!;
    }
}
