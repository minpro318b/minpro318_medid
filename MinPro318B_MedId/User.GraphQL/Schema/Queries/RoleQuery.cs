﻿using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "UserQuery")]
   public class RoleQuery
    {
        private readonly IRoleService _service;
        public RoleQuery(IRoleService service) { _service = service; }
        public async Task<IEnumerable<RoleDto>> GetUserAll()
        {
            IEnumerable<RoleDto> result = await _service.All();
            return result;
        }
        public async Task<RoleDto?> GetUserById(Guid id)
        {
            return await _service.GetRoleById(id);
        }
    }
}
