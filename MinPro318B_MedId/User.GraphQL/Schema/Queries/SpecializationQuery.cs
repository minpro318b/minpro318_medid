﻿using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.Services;

namespace User.GraphQL.Schema.Queries
{
    [ExtendObjectType("Query")]
    public class SpecializationQuery
    {
        public readonly ISpecializationService _service;

        public SpecializationQuery(ISpecializationService service)
        {
            _service = service;
        }

        [UsePaging(IncludeTotalCount = true, DefaultPageSize = 10)] // Cursor-Based Pagination
        public async Task<IEnumerable<SpecializationDto>> GetAllSpecializationAsync()
        {
            IEnumerable<SpecializationDto> result = await _service.All();
            return result;
        }

        [UseOffsetPaging(IncludeTotalCount = true, DefaultPageSize = 10)] // Offset-Based Pagination
        public async Task<IEnumerable<SpecializationDto>> GetOffsetSpecializationAsync()
        {
            IEnumerable<SpecializationDto> result = await _service.All();
            return result;
        }

        public async Task<SpecializationDto> GetSpecializationByIdAsync(Guid id)
        {
            return await _service.GetSpecializationById(id);
        }
    }
}
