﻿using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "UserQuery")]
   public class BiodataQuery
    {
        private readonly IBiodataService _service;
        public BiodataQuery(IBiodataService service) { _service = service; }
        public async Task<IEnumerable<BiodataDto>> GetUserAll()
        {
            IEnumerable<BiodataDto> result = await _service.All();
            return result;
        }
        public async Task<BiodataDto?> GetUserById(Guid id)
        {
            return await _service.GetBiodataById(id);
        }
    }
}
