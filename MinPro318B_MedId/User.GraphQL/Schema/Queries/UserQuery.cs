﻿using MimeKit;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "UserQuery")]
   public class UserQuery
    {
        private readonly IUserService _service;
        private readonly IEmailService _emailService;
        public UserQuery(IUserService service, IEmailService emailService) 
        {
            _service = service;
            _emailService = emailService;
        }
        public async Task<IEnumerable<UserDto>> GetUserAll()
        {
            IEnumerable<UserDto> result = await _service.All();
            return result;
        }
        public async Task<UserDto?> GetUserById(Guid id)
        {
            return await _service.GetUserById(id);
        }
        public async Task<string> IsEmailExist(string email)
        {
            var checkEmail = await _service.IsEmailExist(email);
            if (checkEmail)
                return $"Email : {email} sudah terdaftar";
            else
            {
                var arrint = _service.GenerateOTP(5);
                var arrstr = arrint.Select(arrint => arrint.ToString()).ToArray();
                var otp = String.Join("", arrstr);
                EmailDto dto = new EmailDto
                {
                    To = email,
                    Body = $"Berikut adalah kode otp anda : {otp}",
                    Subject = "Pendaftaran Akun Baru"
                };
                TokenDto tokenDto = new TokenDto
                {
                    Email = email,
                    Token = otp,
                    Expired_On = DateTime.Now.AddMinutes(10),
                    Used_For = "Registration",
                };
                var storeOTP = await _service.StoreOTP(tokenDto);
                if(storeOTP != null)
                {
                    var send = _emailService.SendEmail(dto);
                    if (send)
                        return $"Kode OTP telah dikirimkan ke {email}";
                }
                return "Cobalah beberapa saat lagi";
            }
            return "Terjadi kesalahan, silahkan coba lagi";
        }
    }
}
