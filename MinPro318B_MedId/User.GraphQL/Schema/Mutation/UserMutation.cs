﻿using Framework.Auth;
using User.Domain.Dtos;
using User.Domain.Services;
using User.GraphQL.Inputs;

namespace User.GraphQL.Schema.Mutation
{
    [ExtendObjectType(Name = "UserMutation")]
    public class UserMutation
    {
        private readonly IUserService _service;
        private readonly IBiodataService _biodataService;
        private readonly IRoleService _roleService;
        public UserMutation(IUserService service, IBiodataService biodataService, IRoleService roleService)
        {
            _service = service;
            _biodataService = biodataService;
            _roleService = roleService;
        }
        public async Task<UserDto?> AddUser(int OTP,PasswordInput password, RegistrationInput registration)
        {
            var otp = OTP.ToString();
            var checkOTP = await _service.CheckOtp(otp);
            if (checkOTP)
            {
                UserDto user = new UserDto();
                user.Password = password.ConfirmedPassword;
                user.Email = registration.Email;
                BiodataDto biodataDto = new BiodataDto()
                {
                    Id = new Guid(),
                    FullName = registration.FullName,
                    Mobile_Phone = registration.Mobile_Phone
                };
                await _biodataService.AddBiodata(biodataDto);
                RoleDto roleDto = await _roleService.GetRoleByName(registration.Role_Name);
                user.Role_Id = roleDto.Id;
                user.Biodata_Id = biodataDto.Id;
                return await _service.AddUser(user);
            }
            return null;
        }
        
    }
}
