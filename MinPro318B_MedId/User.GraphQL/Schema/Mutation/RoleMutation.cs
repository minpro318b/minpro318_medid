﻿using Framework.Auth;
using User.Domain.Dtos;
using User.Domain.Services;
using User.GraphQL.Inputs;
using User.GraphQL.Schema.Inputs;

namespace User.GraphQL.Schema.Mutation
{
    [ExtendObjectType(Name = "UserMutation")]
    public class RoleMutation
    {
        private readonly IRoleService _service;
        public RoleMutation(IRoleService service)
        {
            _service = service;
        }
        public async Task<RoleDto> AddRole(RoleInput input)
        {
            RoleDto dto = new RoleDto();
            dto.Name = input.Name;
            dto.Code = input.Code;
            return await _service.AddRole(dto);
        }
        public async Task<RoleDto> UpdateRole(RoleInput input, Guid id)
        {
            RoleDto dto = await _service.GetRoleById(id);
            dto.Name = input.Name;
            dto.Code = input.Code;
            var result = await _service.UpdateRole(dto);
            return result ? dto : throw new GraphQLException(new Error("Role not Found", "404"));
        }
        public async Task<RoleDto> UpdateRoleStatus(Guid id)
        {
            RoleDto dto = await _service.GetRoleById(id);
            var result = await _service.ActivateRole(id);
            return result ? dto : throw new GraphQLException(new Error("Role not Found", "404"));
        }
        public async Task<string> DeleteRole(Guid id)
        {
            RoleDto dto = await _service.GetRoleById(id);
            var result = await _service.DeleteRole(id);
            return result ? dto.Id + " has been deleted": throw new GraphQLException(new Error("Role not Found", "404"));
        }
    }
}
