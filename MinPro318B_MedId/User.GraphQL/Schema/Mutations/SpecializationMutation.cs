﻿using User.Domain;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class SpecializationMutation
    {
        public readonly ISpecializationService _service;

        public SpecializationMutation(ISpecializationService service)
        {
            _service = service;
        }

        public async Task<SpecializationDto> AddSpecializationAsync(SpecializationTypeInput specialization)
        {
            SpecializationDto dto = new SpecializationDto();
            dto.Name = specialization.Name;
            var result = await _service.AddSpecialization(dto);
            return result;
        }

        public async Task<SpecializationDto> EditSpecializationAsync(Guid id, SpecializationTypeInput specialization)
        {
            SpecializationDto dto = new SpecializationDto();
            dto.Id = id;
            dto.Name = specialization.Name;
            var result = await _service.UpdateSpecialization(id, dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Specialization not found", "404"));
            }
            return dto;
        }

        public async Task<SpecializationDto> RemoveSpecializationAsync(Guid id, StatusEnum status)
        {
            var result = await _service.UpdateSpecializationStatus(id, status);
            if (!result)
            {
                throw new GraphQLException(new Error("Specialization not found", "404"));
            }
            return await _service.GetSpecializationById(id);
        }
    }
}
