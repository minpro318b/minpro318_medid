﻿namespace User.GraphQL.Schema.Mutations
{
    public class SpecializationTypeInput
    {
        public string? Name { get; set; }
    }
}
