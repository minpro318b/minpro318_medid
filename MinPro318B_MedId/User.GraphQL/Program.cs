using Microsoft.EntityFrameworkCore;
using User.Domain;
using User.Domain.MapProfile;
using User.Domain.Repositories;
using User.Domain.Services;
using User.GraphQL.Schema.Mutation;
using User.GraphQL.Schema.Queries;

namespace User.GraphQL
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDomainContext(options =>
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(
                    "appsettings.json", optional: true, reloadOnChange: true);
                options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("User_Db_Conn").Value);
                options.EnableSensitiveDataLogging(false).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });
            builder.Services.AddControllers();
            builder.Services.AddAutoMapper(config =>
            {
                config.AddProfile<EntityToDtoProfile>();
            });
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services
                .AddScoped<UserQuery>()
                .AddScoped<UserMutation>()
                .AddScoped<BiodataQuery>()
                //.AddScoped<BiodataMutation>()
                .AddScoped<RoleQuery>()
                .AddScoped<RoleMutation>()
                .AddScoped<IUserRepository, UserRepository>()
                .AddScoped<IUserService, UserService>()
                .AddScoped<IBiodataRepository, BiodataRepository>()
                .AddScoped<IBiodataService, BiodataService>()
                .AddScoped<IRoleRepository, RoleRepository>()
                .AddScoped<IRoleService, RoleService>()
                .AddScoped<IEmailService, EmailService>()
                .AddScoped<ITokenRepository, TokenRepository>()
                .AddGraphQLServer()
                .AddType<UserQuery>()
                .AddType<UserMutation>()
                .AddType<BiodataQuery>()
                .AddType<RoleQuery>()
                .AddType<RoleMutation>()
                .AddQueryType(q => q.Name("UserQuery"))
                .AddMutationType(m => m.Name("UserMutation"));

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapGraphQL();

            app.MapControllers();

            app.Run();
        }
    }
}